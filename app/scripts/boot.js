require.config({
  "baseUrl": "./scripts",
  "paths": {

    "interface": "interface",

    "controller/dashboard" : "modules/dashboard/index",
    "controller/chat"      : "modules/concierge/index",
    "controller/login"     : "modules/login/index",
    "controller/account"   : "modules/account/index",
    "controller/home"      : "modules/home/index",

    "services/geocoder" : "services/geocoder",
    "services/amort"    : "services/amort",

    "directives/ngShowAuth" : "directives/ngShowAuth",
    "directives/ngHideAuth" : "directives/ngHideAuth",
    "filters/reverse"       : "filters/reverse",
    "routes"                : "routes",

    "angular"          : "ext/angular.min",
    "angular-waypoints": "ext/angular-waypoints.min",
    "angular-material" : "ext/angular-material.min",
    "angular-route"    : "ext/angular-route.min",
    "angularAMD"       : "ext/angularAMD.min",
    "ngload"           : "ext/ngload.min",
    "angular-animate"  : "ext/angular-animate.min",
    "angular-cookies"  : "ext/angular-cookies.min",
    "angular-resource" : "ext/angular-resource.min",
    "angular-sanitize" : "ext/angular-sanitize.min",
    "angular-touch"    : "ext/angular-touch.min",
    "angular-aria"    : "ext/angular-aria.min",
    "angular-messages"    : "ext/angular-messages.min",
    "angular-storage"  : "ext/ngStorage",
    "angularfire"      : "ext/angularfire.min",
    "firebase"         : "ext/firebase",
    "lodash" : "ext/lodash.min",

    "leaflet": "ext/leaflet",
    "osm": "ext/OSMBuildings-Leaflet",
    "angular-leaflet-directive": "ext/angular-leaflet-directive.min",
    "esri-leaflet-geocoder": "ext/esri-leaflet-geocoder",
    "esri-leaflet": "ext/esri-leaflet",
    "d3": "ext/d3.min",
    "jquery": "ext/jquery-1.9.1.min",
    "jquery.unveil": "ext/jquery.unveil.min"
  },

  "shim": {
    "jquery.unveil": [
      'jquery'
    ],
    "esri-leaflet": ['angular-leaflet-directive'],
    "esri-leaflet-geocoder": ["esri-leaflet"],
    "lodash": {
      "exports": "_"
    },
    "angular": {
      "exports": "angular"
    },
    "angular-waypoints": [
      "angular"
    ],
    "angular-material": [
      'angular'
    ],
    "angular-route": [
      "angular"
    ],
    "angularAMD": [
      "angular"
    ],
    "ngload": [
      "angularAMD"
    ],
    "angular-aria": [
      "angular"
    ],
    "angular-messages": [
      "angular"
    ],
    "angular-animate": [
      "angular"
    ],
    "angular-cookies": [
      "angular"
    ],
    "angular-resource": [
      "angular"
    ],
    "angular-sanitize": [
      "angular"
    ],
    "angular-touch": [
      "angular"
    ],
    "angularfire": [
      "angular",
      "firebase"
    ],
    "leaflet": {
      "deps": ["angular"],
      "exports": "L"
    },
    "osm": ['angular-leaflet-directive'],
    "angular-leaflet-directive": ['angular', 'leaflet']

  },

  "deps": ['interface']
});
