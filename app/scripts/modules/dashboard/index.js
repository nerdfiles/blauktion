'use strict';

/**
 * @ngdoc function
 * @name blockauction.controller:MainController
 * @description
 * # MainController
 * Main Controller — probably for a Splash screen that isn't "Home"...
 */
define([
  "interface"
], function (__interface__) {
  var MainController = function ($scope) {

  };

  return [
    "$scope",
    MainCtrl
  ];
});
