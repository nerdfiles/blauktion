define(['interface'], function (__interface__) {
  function Encounter ($q) {

    var modelInterface = {};

    modelInterface.initialize = function () {
      var def = $q.defer();
      modelInterface.initialized = true;
      def.resolve(modelInterface);
      return def.promise;
    };

    modelInterface.initialize().then(function (data) {
      console.log(data);
    });

    return modelInterface;
  }

  __interface__.factory('Encounter', [
    '$q',
    Encounter
  ]);

});
