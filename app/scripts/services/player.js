define(['interface'], function () {

  function Player ($q) {
    /***
     *
     */
    var modelInterface = {
      pause: function () {}
    };
    return modelInterface;
  }
  __interface__.factory('Player', [
    '$q',
    Player
  ]);
});
