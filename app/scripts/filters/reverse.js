'use strict';

define(['angularAMD'], function (angularAMD) {
  angularAMD.filter('reverse', function() {
    return function(items) {
      return angular.isArray(items)? items.slice().reverse() : [];
    };
  });
});
