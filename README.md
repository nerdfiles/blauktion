# Block Auction Project

## Setup

Run `grunt setup` to install dependencies for the front end.

## Build & development

Run `grunt` for building and `grunt devel` for development

## Serving Locally

Run the local server with `node server/www.js`.

## Style Compilation

Move into the `app/styles/` folder and run `sass --watch sass:css`

## Testing

Running `grunt test` will run the unit tests with karma.

## Management

### “Building” content from static site copy

Content Authors (CA) may use http://prose.io to update the `contents` directory.

This project uses [Punch](https://github.com/laktek/punch/wiki) to convert 
Moustache templates into static site pages that essentially map public URLs 
to files created in the repository. Github, effectively, and logical naming of,
files becomes the CMS. This should happen during the Build phase (Grunt).

## Features

1. Bidder Transaction Types: Peel, Double Peel, Max+1, Quick+1
2. Product Views: Detail, List, etc  
   Must scrape existing heritageauction site.
3. Cam Screens/Directives  
   Shared view for cameras.
4. Login  
   Subscription/Auth Mode
5. Paypal and Stripe Integration 
6. Chat Concierge/Help
7. 

## Current Sitemap

1. Home/Search  
   (Educate, Start Search): Use Mixpanel tracking in AngularJS to track hover, keydown, mousedown, scroll, visibility of item within viewport bounds. So virtual data is “watching all customers” to collect info on them.
2. Chat  
   (Simple) automated chat with conversational UI as “sequences of questions user would choose on other parts of the site” but in “text flow” with momentary data (relative to other data) inquiry-response.
3. Login  
   A login screen that stores username optionally.
4. Account (Deals, etc.)  
   Following Ontology: Account screen shows “following ontology” or Respect Matrix view with lists, dashboards, etc. from timeseries graphs as the item logs certain aggregate/macro events from Mixpanel 
   Reactive Bidding Histories: First “callout lead paragraph” of the page implements [Tangle](http://worrydream.com/Tangle/) to produce reactive bidding history look-and-feel. So unfortunately I don’t have 
   a ready place to design the bidding history and fit them as content — but I think the reactive “account” overview would be clean solution to showing value in an interactive, 
   projective heuristic. The text summary becomes an interactive “textualization” (visualization) of ROI with a blob of text. Testmonials are like “templates” we inherit 
   to demonstrate the “flow of the conversation” perhaps in the vendor’s own language/vernacular. So we could do natural language processing to identify common language 
   terms to make hookable in tangle. Must be quantitatively/statistically enforcing terms.

## Map Tool

AuctionCoin (P, C) pairings are displayed on the map at LatLngs. Monthly 
payments from bidder updates a counter for the map marker that represents P.

1. Aged markers  
   Newer pairings will appear hotter until the "flame" fizzles out.
2. Annotated markers  
   Some Auctions have notes. Render these in a bottom sheet for markers.
3. Bid Latest / Payment Conformance markers  
   You can see the item with a countdown of payments. Age from start  
   of Auction. Assertion of Payment Performance which may extend behind age.  
   Age is a constant that becomes less visible as successive payments are 
   met.
4. Profit Positive markers  
   Bidders who have extended their payment obligations will augment  
   markers with an icon that represents earnings beyond initial  
   auction offering differential of total interest with new total interest.
5. Multisig markers  
   Shared Auction: A marker may have two bidders with one Auction. If the users opt-in  
   for the Auction, the marker appears. One signed bidder will determine  
   the marker "inactive" or "pending".  
   Flagging: A marker may receive reports from all parties on the validity of the  
   item.
6. Chained markers  
   Vendor Ownership Influence: Vendor has multiple auctions that creates a chained network. If same LatLng, Spiderify/Cluster.  
   bidder Chained Ownership: One bidder has multiple auctions for multiple estates.
7. Voting Sentiments on markers  
   Bidders will "like" a property with a certain mood description.
8. Business Card markers  
   Bidders can attach business cards to a marker if the pairing exists.
9. Activity markers  
   Item may appear without a pairing but can be viewed by all. Pairings  
   only appear to bidders who are logged in.  
   Activity depends on engagement model (clicks, views, etc.) of item  
   Web views.
10. Chatty markers  
    Public markers instantiate chatrooms.
11. Published markers  
    Bidders may publish their successful or unsuccessful pairings.
12. Invitation markers  
    Bidders may invite other users to view their pairings. Or invites to Request for Auction.
13. Clerk Updates / Clerk Context Menus  
    Accessing limited functionality and info (history) of managed Web kiosk. Clerk is access 
    point to financial services which have hitherto been available only to high end customers 
    of banked persons.
14. Digital Notary / Digital IOUs?  
    Service to document assets. Assets stored under map marker.
