
/**
@fileOverview ./blockauction/theme.js
@description
Load blockauction theme.
 */

var
express = require('express'),
path = require('path');

var themeRouteConfig = function(__interface__) {

  /**
  Theme Route Config
  @module blockauction.app/theme
   */
  __interface__.app.use(function(req, res, next) {
      res.setHeader("Content-Security-Policy", "default-src 'none'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self';");
      return next();
  });
  __interface__.app.use(express["static"](path.join(__dirname, '../app')));
  __interface__.app.use('/assets', express["static"](path.join(__dirname, '../app')));
  return __interface__;
};

module.exports = themeRouteConfig;
