var
mongoose = require('mongoose'),
Schema = mongoose.Schema;

var
AuctionLot = new Schema({
  _id: Number,
  name: String,
  items: [
    {
      type: Schema.Types.ObjectId,
      ref: 'AuctionItem'
    }
  ]
}),

auctionLot = mongoose.model("AuctionLot", AuctionLot);

module.exports = {
  auctionLot: auctionLot
};
