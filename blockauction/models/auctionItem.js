var
mongoose = require('mongoose'),
Schema = mongoose.Schema,

product = new Schema({
  name: {
    type: String,
    required: true
  },
  body: {
    type: String,
    required: true
  },
  self: {
    type: String,
    required: true
  },
  imageUrl: {
    type: String,
    required: true
  },
  datePublished: {
    type: Date
  },
  comment: {
    type: String
  }
}, {
  _id: false
}),

aggregateRating = new Schema({
  ratingValue: {
    type: String
  },
  reviewCount: {
    type: Number
  }
}, {
  _id: false
}),

PriceSpecification = new Schema({
  _id: Number,
  price: Number,
  minPrice: Number,
  maxPrice: Number,
  priceCurrency: String,
  validFrom: Date,
  validThrough: Date
});

priceSpecification = mongoose.model("PriceSpecification", PriceSpecification);

var Bid = new Schema({
  priceSpecification: {
    type: Schema.Types.ObjectId,
    ref: 'PriceSpecification'
  },
  _itemOffered: {
    type: Number,
    ref: 'AuctionItem'
  },
  self: {
    type: String
  }
}),

bid = mongoose.model("Bid", Bid);

var AuctionItem = new Schema({
  _id: {
    type: Number
  },
  self: {
    type: String
  },
  description: {
    type: String,
    required: true
  },
  availability: {
    type: String
  },
  status: {
    type: String,
    required: true
  },
  bids: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Bid'
    }
  ],
  templates: [product, aggregateRating]
}),

auctionItem = mongoose.model("AuctionItem", AuctionItem);

module.exports = {
  auctionItem: auctionItem
};
