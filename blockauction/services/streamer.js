var
request = require('request'),
http = require('http'),
fs = require('fs'),
q = require('q'),
EE = require("events").EventEmitter,
ee = new EE();

http.createServer(function(req, res) {
  ee.on("yt", function(id) {
    var def, x;
    def = q.defer();
    x = request('http://www.youtube.com/embed/' + id);
    def.resolve(x);
  });
  req.pipe(x);
  return x.pipe(res);
}).listen(1337, '127.0.0.1');

var stream = function(id) {
  return ee.emit("yt", "thnSuPz66qk");
};

module.exports = stream;
