var e, error, q;

var
util = require('util'),
OperationHelper = require('apac').OperationHelper,
path = require('path'),
fs = require('fs');

try {
  q = require('Q');
} catch (error) {
  e = error;
  console.log(e);
  q = require('q');
}

var awsCredentials = JSON.parse(fs.readFileSync(path.join(__dirname, 'awsCredentials.json')));
var opHelper = new OperationHelper(awsCredentials);
var product = function() {
  var def;
  def = q.defer();
  opHelper.execute('ItemSearch', {
    'SearchIndex': 'Books',
    'Keywords': 'Bitcoin',
    'ResponseGroup': 'ItemAttributes,Offers'
  }, function(err, results) {
    def.resolve(results);
  });
  return def.promise;
};

module.exports = product;
