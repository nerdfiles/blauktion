
/**
@fileOverview ./blockauction/caps.js
@description
1. Documentation
2. API Mocks
 */

var
express = require('express'),
path = require('path');

var middlewareConfig = function(__interface__) {

  /**
  Dev Route Config
  @module blockauction.app/dev
   */
  __interface__.app.use('/bower_components', express["static"](path.join(__dirname, '../bower_components')));
  return __interface__;
};

module.exports = middlewareConfig;
