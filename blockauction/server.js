
/**
@fileOverview ./blockauction/server.js
 */

var
bodyParser = require('body-parser'),
express = require('express'),
http = require('http'),
path = require('path'),
methodOverride = require('method-override'),
mongoose = require('mongoose'),
restify = require('express-restify-mongoose'),
auctionItem = require('./models/auctionItem'),
auctionLot = require('./models/auctionLot'),
config,
server;

config = {
  port: process.env['PORT'] || 3005
};

server = function(app) {
  var auctionItemModel, auctionLotModel, connectExpressServer, e, error, router;
  router = express.Router();
  //mongoose.connect('mongodb://localhost/blockauction');
  mongoose.connect('mongodb://heroku_qw6sqvgv:heroku_qw6sqvgv@ds015720.mlab.com:15720/heroku_qw6sqvgv');
  auctionItemModel = auctionItem.auctionItem;
  auctionLotModel = auctionLot.auctionLot;
  try {
    restify.serve(router, auctionItemModel);
    restify.serve(router, auctionLotModel);
  } catch (error) {
    e = error;
    console.log(e);
  }
  app.use(router);
  app.set('port', config.port);
  app.set('views', path.join(__dirname, '../app'));
  app.engine('html', require('ejs').renderFile);
  app.set('view engine', 'html');
  app.use(bodyParser.json());
  app.use(methodOverride());
  connectExpressServer = function() {

    /**
    Initialize Application Server
     */
    return server = http.createServer(app);
  };
  return {
    app: app,
    server: connectExpressServer()
  };
};

module.exports = server;
