/* global describe, it */

(function () {
  'use strict';

  describe('Home', function () {
    describe('product search', function () {
      it('should fetch a few products (AuctionItems)', function () {
        var request = require('request');

        request({
          url: '/api/v1/AuctionItem',
          qs: {
            query: JSON.stringify({
              $or: [{
                name: '~Another'
              }, {
                $and: [{
                  name: '~AuctionItem'
                }, {
                  price: '<=10'
                }]
              }],
              price: 20
            })
          }
        });
      });
    });
  });
})();
