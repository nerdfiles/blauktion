/*
@fileOverview ./server/www.coffee
@description
The Web is fundamentally a distributed hypermedia application.
*/

var
Promise = require('bluebird'),
express = require('express'),
app = 'blockauction';

Promise.resolve(express()).
  then(require('../' + app + '/server')).
  then(require('../' + app + '/interface')).
  then(require('../' + app + '/middleware')).
  then(require('../' + app + '/theme')).
  then(require('../' + app + '/start'));

